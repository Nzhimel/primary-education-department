package com.nationalappsbd.primaryeducationdepartment.interfaces;

/**
 * Created by shuvojit on 5/14/15.
 */
public interface MenuIDsInterface {

    public final static String INTERNAL_OFFICES = "1053";

    public final static String HOME_PAGE_ID = "1038";

    public final static String PROJECT_ID = "1050";

    public final static String PUBLICATIONS_AND_NEWS = "1051";

    public final static String RULES = "1054";

    public final static String NOTICE_BOARD = "1049";

    public final static String ABOUT_US = "1074";

    public final static String INFORIGHTS = "1077";

    public final static String NEWS_UPDATE = "1085";

    public final static String NEWS_AND_OTHERS = "1086";

}
