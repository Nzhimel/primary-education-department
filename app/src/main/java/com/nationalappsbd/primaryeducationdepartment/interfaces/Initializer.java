package com.nationalappsbd.primaryeducationdepartment.interfaces;

/**
 * Created by shuvojit on 5/6/15.
 */
public interface Initializer {

    public void initialize();
}
