package com.nationalappsbd.primaryeducationdepartment.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PhotoGallery;
import com.nationalappsbd.primaryeducationdepartment.R;
import com.nationalappsbd.primaryeducationdepartment.activities.FragmentActivity;
import com.nationalappsbd.primaryeducationdepartment.adapters.gridViewAdapters.PrimaryEduInfoGridViewAdapter;
import com.nationalappsbd.primaryeducationdepartment.infos.ConnectivityInfo;
import com.nationalappsbd.primaryeducationdepartment.interfaces.Initializer;

import java.util.List;

/**
 * Created by shuvojit on 5/24/15.
 */
public class MainPageFragment extends Fragment implements Initializer,
        AdapterView.OnItemClickListener{


    private Context context;
    private View fragmentView;
    private List<PhotoGallery> photoGalleryList;
    private int[] imageResources;
    private GridView gridView;
    private String[] subMenus;


    public MainPageFragment() {
    }

    public static MainPageFragment getNewInstance() {
        return new MainPageFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        LayoutInflater layoutInflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fragmentView = layoutInflater.inflate(R.layout.first_page_fragment_layout, null, false);
        initialize();
        gridView.setOnItemClickListener(this);
        return fragmentView;
    }

    @Override
    public void initialize() {
        if (fragmentView != null) {
            photoGalleryList = PhotoGallery.photoGalleryList();
            TypedArray typedArray = context.getResources().obtainTypedArray(R.array.photo_gallery);
            if (typedArray != null && typedArray.length() > 0) {
                imageResources = new int[typedArray.length()];
                for (int i = 0; i < typedArray.length(); i++) {
                    imageResources[i] = typedArray.getResourceId(i, 0);
                }
                typedArray.recycle();
            }
            setImageSlider();
            gridView = (GridView) fragmentView.findViewById(R.id.gridView);
            subMenus = context.getResources().getStringArray(R.array.main_page_submenus);
            PrimaryEduInfoGridViewAdapter primaryEduInfoGridViewAdapter = new
                    PrimaryEduInfoGridViewAdapter(context, subMenus);
            if (primaryEduInfoGridViewAdapter != null) {
                gridView.setAdapter(primaryEduInfoGridViewAdapter);
            }
        }
    }

    private void setImageSlider() {
        SliderLayout imageSliderLayout = (SliderLayout) fragmentView
                .findViewById(R.id.image_slider);
        for (int i = 0; i < imageResources.length; i++) {
            TextSliderView textSliderView = new TextSliderView(context);
            textSliderView
                    .description(photoGalleryList.get(i).getDescription())
                    .image(imageResources[i])
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            imageSliderLayout.addSlider(textSliderView);
        }
        if (ConnectivityInfo.isInternetConnectionOn(context)) {
            for (int i = imageResources.length; i < photoGalleryList.size(); i++) {
                PhotoGallery photoGallery = photoGalleryList.get(i);
                if (!photoGallery.getImageLink().equals("") ||
                        photoGallery.getImageLink() != null) {
                    TextSliderView textSliderView = new TextSliderView(context);
                    textSliderView
                            .description(photoGallery.getDescription())
                            .image(photoGallery.getImageLink())
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                    imageSliderLayout.addSlider(textSliderView);
                }
            }
        }
        imageSliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        imageSliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        imageSliderLayout.setCustomAnimation(new DescriptionAnimation());
        imageSliderLayout.setDuration(5000);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(context, FragmentActivity.class);
        intent.putExtra("Fragment Name", subMenus[position]);
        context.startActivity(intent);

    }
}
