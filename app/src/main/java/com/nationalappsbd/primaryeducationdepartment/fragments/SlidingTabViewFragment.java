package com.nationalappsbd.primaryeducationdepartment.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Statictics;
import com.nationalappsbd.primaryeducationdepartment.R;
import com.nationalappsbd.primaryeducationdepartment.adapters.fragmentAdapters.FragmentAdapter;
import com.nationalappsbd.primaryeducationdepartment.interfaces.Initializer;
import com.nationalappsbd.primaryeducationdepartment.tabs.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shuvojit on 5/7/15.
 */
public class SlidingTabViewFragment extends Fragment implements Initializer {


    private String FRAGMENT_SHOW_TYPE = null;
    private Context context;
    private ArrayList<Fragment> fragmentArrayList;
    private View fragmentView;
    private SlidingTabLayout slidingTabLayout;
    private FragmentAdapter fragmentAdapter;
    private ViewPager viewPager;
    private String[] viewPagerNames;


    public static Fragment getNewInstance(final String MENU_NAME) {
        Bundle bundle = new Bundle();
        bundle.putString("FRAGMENT_SHOW_TYPE", MENU_NAME);
        SlidingTabViewFragment slidingTabViewFragment = new SlidingTabViewFragment();
        slidingTabViewFragment.setArguments(bundle);
        return slidingTabViewFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            context = getActivity();
            Bundle bundle = getArguments();
            FRAGMENT_SHOW_TYPE = bundle.getString("FRAGMENT_SHOW_TYPE");
            switch (FRAGMENT_SHOW_TYPE) {
                case "মূলপাতা":
                    setFragmentForMainPage();
                    break;
                case "আমাদের সম্পর্কে":
                    setFragmentsForAboutPrimaryEduDept();
                    break;
                case "অধীনস্ত অফিসসমূহ":
                    setFragmentForPrimaryEduDeptInternalOffices();
                    break;
                case "তথ্য অধিকার":
                    setFragmentsForInformationRights();
                    break;
                case "আইন ও বিধিমালা":
                    setFragmentForLowAndRules();
                    break;
                case "সংবাদ ও অন্যান্য":
                    setFragmentForNewsAndAdvertisement();
                    break;
                case "প্রকল্প":
                    setFragmentForProject();
                    break;
                case "প্রকাশনা ও অন্যান্য":
                    setFragmentsForPublications();
                    break;
            }
        }
    }

    private void setFragmentsForPublications() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(10, "School Result");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(10, "Madrasha Result");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(10, "Investigation");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(10, "Publications");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(10, "Forms");
        fragmentArrayList.add(fragment);
        List<Statictics> staticticsList = Statictics.getStaticticsList();
        String pageNames[] = new String[5];
        if (staticticsList != null) {
            pageNames[0] = staticticsList.get(0).getSubMenu();
            pageNames[1] = staticticsList.get(1).getSubMenu();
        }
        int j = 2;
        for (int i = 0; i < context
                .getResources()
                .getStringArray(R.array.publications_subMenu)
                .length; i++) {
            pageNames[j] = context
                    .getResources()
                    .getStringArray(R.array.publications_subMenu)[i];
            j++;
        }
        viewPagerNames = pageNames;
    }

    private void setFragmentForProject() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(9, "Project");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(17, "Project 2");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.project_subMenus);
    }

    private void setFragmentForNewsAndAdvertisement() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(8, "Tender");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.news_and_advertisement);
    }

    private void setFragmentForMainPage() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = MainPageFragment.getNewInstance();
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(5, "director general");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.main_page);
    }

    private void setFragmentsForInformationRights() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(3, "information rights");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(3, "information providing officer");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(3, "givense redress system");
        fragmentArrayList.add(fragment);
        viewPagerNames = context
                .getResources()
                .getStringArray(R.array.information_rights);

    }

    private void setFragmentForLowAndRules() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(6, "rules");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(6, "advertisement");
        fragmentArrayList.add(fragment);
        viewPagerNames = context.getResources().getStringArray(R.array.law_rules);
    }

    private void setFragmentForPrimaryEduDeptInternalOffices() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        viewPagerNames = context.getResources().getStringArray(R.array.internel_offices);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(2, "Division Office");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(2, "District Office");
        fragmentArrayList.add(fragment);
        /*fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(2, "PTI Office");
        fragmentArrayList.add(fragment);*/
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(18, "E Primary System");
        fragmentArrayList.add(fragment);

    }

    private void setFragmentsForAboutPrimaryEduDept() {
        fragmentArrayList = new ArrayList<Fragment>();
        Fragment fragment = null;
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(1, "Primary Education Department");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(1, "History");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(1, "Future Plan");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(1, "Director Generals Working Period");
        fragmentArrayList.add(fragment);
        fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                .getNewInstance(1, "Working Procedure");
        fragmentArrayList.add(fragment);
        viewPagerNames = context
                .getResources()
                .getStringArray(R.array.about_primary_education_dept);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = null;

        if (savedInstanceState == null) {
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            fragmentView = inflater.inflate(R.layout.view_pager_includer_fragment_layout,
                    container, false);
            initialize();
            setFragmentAdapter();
        }

        return fragmentView;
    }

    private void setFragmentAdapter() {
        if (viewPager != null && fragmentAdapter != null) {
            viewPager.setAdapter(fragmentAdapter);
            slidingTabLayout.setDistributeEvenly(true);
            slidingTabLayout.setViewPager(viewPager);
            slidingTabLayout.setDistributeEvenly(true);
            slidingTabLayout.setSelectedIndicatorColors(getResources()
                    .getColor(R.color.yellow));
        }
    }

    @Override
    public void initialize() {
        if (fragmentView != null) {
            viewPager = (ViewPager) fragmentView.findViewById(R.id.scroll_view_pager);
            slidingTabLayout = (SlidingTabLayout) fragmentView.findViewById(R.id.slidingTab);
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null && fragmentArrayList != null && viewPagerNames != null) {
                fragmentAdapter = new FragmentAdapter(fragmentManager,
                        fragmentArrayList, viewPagerNames);
            }
        }
    }
}
