package com.nationalappsbd.primaryeducationdepartment.adapters.gridViewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nationalappsbd.primaryeducationdepartment.R;

/**
 * Created by shuvojit on 5/10/15.
 */
public class PrimaryEduInfoGridViewAdapter extends BaseAdapter {


    private Context context;
    private LayoutInflater layoutInflater;
    private String[] subMenus;


    public PrimaryEduInfoGridViewAdapter(Context context, String[] subMenus) {
        this.context = context;
        this.subMenus = subMenus;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        int size = 0;
        size = subMenus.length;
        return size;
    }

    @Override
    public Object getItem(int position) {
        Object object = null;
        object = subMenus[position];
        return object;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View adapterView = null;
        if (convertView == null) {

            adapterView = layoutInflater.inflate(R.layout.text_view_layout, null, false);

        } else {
            adapterView = convertView;

        }
        TextView textView = (TextView) adapterView.findViewById(R.id.txt_office_location);
        textView.setText(subMenus[position]);
        return adapterView;
    }


}
