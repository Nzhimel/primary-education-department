package com.nationalappsbd.primaryeducationdepartment.adapters.listViewAdapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Advertisement;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.CentralEServices;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.DistrictOffice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.DivisionOffice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.E_PrimarySystem;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Forms;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.GivenseRedressSystem;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.ImportantLink;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.InfoProvidingOfficer;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.InformationRights;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.InternalServices;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.MadrashaResultStatictics;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.MonthlyInvestigation;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.NewsUpdate;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Notice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.OfficersNameList;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.ProjectPedp3;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PtiOffice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Publications;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.Rules;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.SchoolResultStatictics;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.TenderNotice;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.WorkingPeriodDirectorGeneral;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.WorkingProcedure;
import com.nationalappsbd.primaryeducationdepartment.R;
import com.nationalappsbd.primaryeducationdepartment.activities.FragmentActivity;
import com.nationalappsbd.primaryeducationdepartment.dialogs.UserNotifiedDialog;
import com.nationalappsbd.primaryeducationdepartment.infos.ConnectivityInfo;
import com.nationalappsbd.primaryeducationdepartment.webServices.WebBrowser;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by shuvojit on 5/20/15.
 */
public class PrimaryEduDeptInfoListViewAdapter extends BaseAdapter {
    private String[] categoriesName;
    private String[] detailsName;
    private int[] officersImages;
    private int LIST_VIEW_SHOW_TYPE;
    private Context context;
    private LayoutInflater layoutInflater;
    private List<WorkingPeriodDirectorGeneral> workingPeriodDirectorGeneralList;
    private List<WorkingProcedure> workingProcedureList;
    private List<DivisionOffice> divisionOfficeList;
    private List<DistrictOffice> districtOfficeList;
    private List<PtiOffice> ptiOfficeList;
    private List<E_PrimarySystem> e_primarySystems;
    private List<InformationRights> informationRightsList;
    private List<InfoProvidingOfficer> infoProvidingOfficerList;
    private List<GivenseRedressSystem> givenseRedressSystemList;
    private List<NewsUpdate> newsUpdateList;
    private List<Rules> rulesList;
    private List<Notice> noticeList;
    private List<Advertisement> advertisementList;
    private List<TenderNotice> tenderNoticeList;
    private List<ProjectPedp3> projectPedp3List;
    private List<MonthlyInvestigation> monthlyInvestigationList;
    private List<Forms> formsList;
    private List<Publications> publicationsList;
    private List<SchoolResultStatictics> schoolResultStaticticsList;
    private List<MadrashaResultStatictics> madrashaResultStaticticsList;
    private List<InternalServices> internalServicesList;
    private List<CentralEServices> centralEServicesList;
    private List<ImportantLink> importantLinkList;
    private List<OfficersNameList> officersNameListList;


    //LIST_VIEW_SHOW_TYPE = 1 (Working Period Of Director Generals)
    //LIST_VIEW_SHOW_TYPE = 2 (Working Procedure)
    //LIST_VIEW_SHOW_TYPE = 3 (Division Office)
    //LIST_VIEW_SHOW_TYPE = 4 (District Office)
    //LIST_VIEW_SHOW_TYPE = 5 (PTI Office)
    //LIST_VIEW_SHOW_TYPE = 6 (E_Primary_System)
    //LIST_VIEW_SHOW_TYPE = 7 (Information Rights)
    //LIST_VIEW_SHOW_TYPE = 8 (Information Providing Officer)
    //LIST_VIEW_SHOW_TYPE = 9 (Givense Redress System)
    //LIST_VIEW_SHOW_TYPE = 10 (News update)
    //LIST_VIEW_SHOW_TYPE = 11 (Rules)
    //LIST_VIEW_SHOW_TYPE = 12 (Advertisement)
    //LIST_VIEW_SHOW_TYPE = 13 (Notice)
    //LIST_VIEW_SHOW_TYPE = 14 (Tender Notice)
    //LIST_VIEW_SHOW_TYPE = 15 (Project PEDP 3)
    //LIST_VIEW_SHOW_TYPE = 16 (School Result)
    //LIST_VIEW_SHOW_TYPE = 17 (Madrasha Result)
    //LIST_VIEW_SHOW_TYPE = 18 (Investigations)
    //LIST_VIEW_SHOW_TYPE = 19 (Publications)
    //LIST_VIEW_SHOW_TYPE = 20 (Forms)
    //LIST_VIEW_SHOW_TYPE = 21 (Central E services)
    //LIST_VIEW_SHOW_TYPE = 22 (Internal services)
    //LIST_VIEW_SHOW_TYPE = 23 (Important Link)
    //LIST_VIEW_SHOW_TYPE = 24 (Officers List)


    public PrimaryEduDeptInfoListViewAdapter(Context context,
                                             int LIST_VIEW_SHOW_TYPE, List primaryEduInfoList) {
        this.context = context;
        layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.LIST_VIEW_SHOW_TYPE = LIST_VIEW_SHOW_TYPE;
        TypedArray typedArray = null;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                workingPeriodDirectorGeneralList = primaryEduInfoList;
                break;
            case 2:
                workingProcedureList = primaryEduInfoList;
                break;
            case 3:
                divisionOfficeList = primaryEduInfoList;
                break;
            case 4:
                districtOfficeList = primaryEduInfoList;
                break;
            case 5:
                ptiOfficeList = primaryEduInfoList;
                break;
            case 6:
                e_primarySystems = primaryEduInfoList;
                break;
            case 7:
                informationRightsList = primaryEduInfoList;
                break;
            case 8:
                infoProvidingOfficerList = primaryEduInfoList;
                break;
            case 9:
                givenseRedressSystemList = primaryEduInfoList;
                break;
            case 10:
                newsUpdateList = primaryEduInfoList;
                break;
            case 11:
                rulesList = primaryEduInfoList;
                break;
            case 12:
                advertisementList = primaryEduInfoList;
                break;
            case 13:
                noticeList = primaryEduInfoList;
                break;
            case 14:
                tenderNoticeList = primaryEduInfoList;
                break;
            case 15:
                projectPedp3List = primaryEduInfoList;
                break;
            case 16:
                schoolResultStaticticsList = primaryEduInfoList;
                break;
            case 17:
                madrashaResultStaticticsList = primaryEduInfoList;
                break;
            case 18:
                monthlyInvestigationList = primaryEduInfoList;
                break;
            case 19:
                publicationsList = primaryEduInfoList;
                break;
            case 20:
                formsList = primaryEduInfoList;
                break;
            case 21:
                centralEServicesList = primaryEduInfoList;
                break;
            case 22:
                internalServicesList = primaryEduInfoList;
                break;
            case 23:
                importantLinkList = primaryEduInfoList;
                break;
            case 24:
                officersNameListList = primaryEduInfoList;
                typedArray = context.getResources().obtainTypedArray(R.array.officers_image);
                if (typedArray != null && typedArray.length() == officersNameListList.size()) {
                    officersImages = new int[typedArray.length()];
                    for (int i = 0; i < officersImages.length; i++) {
                        officersImages[i] = typedArray.getResourceId(i, 0);
                    }
                    typedArray.recycle();
                }
                break;
            default:
                break;
        }
    }


    @Override
    public int getCount() {
        int count = 0;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                count = workingPeriodDirectorGeneralList.size();
                break;
            case 2:
                count = workingProcedureList.size();
                break;
            case 3:
                count = divisionOfficeList.size();
                break;
            case 4:
                count = districtOfficeList.size();
                break;
            case 5:
                count = ptiOfficeList.size();
                break;
            case 6:
                count = e_primarySystems.size();
                break;
            case 7:
                count = informationRightsList.size();
                break;
            case 8:
                count = infoProvidingOfficerList.size();
                break;
            case 9:
                count = givenseRedressSystemList.size();
                break;
            case 10:
                count = newsUpdateList.size();
                break;
            case 11:
                count = rulesList.size();
                break;
            case 12:
                count = advertisementList.size();
                break;
            case 13:
                count = noticeList.size();
                break;
            case 14:
                count = tenderNoticeList.size();
                break;
            case 15:
                count = projectPedp3List.size();
                break;
            case 16:
                count = schoolResultStaticticsList.size();
                break;
            case 17:
                count = madrashaResultStaticticsList.size();
                break;
            case 18:
                count = monthlyInvestigationList.size();
                break;
            case 19:
                count = publicationsList.size();
                break;
            case 20:
                count = formsList.size();
                break;
            case 21:
                count = centralEServicesList.size();
                break;
            case 22:
                count = internalServicesList.size();
                break;
            case 23:
                count = importantLinkList.size();
                break;
            case 24:
                count = officersNameListList.size();
                break;
            default:
                break;

        }

        return count;
    }

    @Override
    public Object getItem(int position) {
        Object object = null;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                object = workingPeriodDirectorGeneralList.get(position);
                break;
            case 2:
                object = workingProcedureList.get(position);
                break;
            case 3:
                object = divisionOfficeList.get(position);
                break;
            case 4:
                object = districtOfficeList.get(position);
                break;
            case 5:
                object = ptiOfficeList.get(position);
                break;
            case 6:
                object = e_primarySystems.get(position);
                break;
            case 7:
                object = informationRightsList.get(position);
                break;
            case 8:
                object = infoProvidingOfficerList.get(position);
                break;
            case 9:
                object = givenseRedressSystemList.get(position);
                break;
            case 10:
                object = newsUpdateList.get(position);
                break;
            case 11:
                object = rulesList.get(position);
                break;
            case 12:
                object = advertisementList.get(position);
                break;
            case 13:
                object = noticeList.get(position);
                break;
            case 14:
                object = tenderNoticeList.get(position);
                break;
            case 15:
                object = projectPedp3List.get(position);
                break;
            case 16:
                object = schoolResultStaticticsList.get(position);
                break;
            case 17:
                object = madrashaResultStaticticsList.get(position);
                break;
            case 18:
                object = monthlyInvestigationList.get(position);
                break;
            case 19:
                object = publicationsList.get(position);
                break;
            case 20:
                object = formsList.get(position);
                break;
            case 21:
                object = centralEServicesList.get(position);
                break;
            case 22:
                object = internalServicesList.get(position);
                break;
            case 23:
                object = importantLinkList.get(position);
                break;
            case 24:
                object = officersNameListList.get(position);
                break;
        }

        return object;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View adapterView = null;
        if (convertView == null && layoutInflater != null) {
            switch (LIST_VIEW_SHOW_TYPE) {
                case 1:
                    adapterView = layoutInflater.inflate
                            (R.layout.working_period_of_director_generals_layout, null, false);
                    break;
                case 2:
                    adapterView = layoutInflater.inflate
                            (R.layout.primary_education_info_necessary_info_layout, null, false);
                    break;
                case 6:
                case 7:
                case 9:
                    adapterView = layoutInflater.inflate(R.layout.office_location_name_layout,
                            null, false);
                    break;
                case 3:
                    adapterView = layoutInflater.inflate(R.layout.division_layout,
                            null, false);
                    break;
                case 4:
                case 5:
                    adapterView = layoutInflater.inflate(R.layout.info_providing_officer_layout,
                            null, false);
                    break;
                case 8:
                    adapterView = layoutInflater.inflate(R.layout.info_view_layout, null, false);
                    break;
                case 10:
                case 14:
                    adapterView = layoutInflater.inflate(R.layout.category_details_layout,
                            null, false);
                    break;
                case 11:
                case 12:
                case 13:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                    adapterView = layoutInflater.inflate(R.layout.office_location_name_layout,
                            null, false);
                    break;
                case 24:
                    adapterView = layoutInflater.inflate(R.layout.officers_list_layout,
                            null, false);
                    break;
                default:
                    break;
            }
        } else {
            adapterView = convertView;
        }
        setRequiredFields(adapterView, position);
        return adapterView;
    }

    private void setRequiredFields(View adapterView, int position) {
        switch (LIST_VIEW_SHOW_TYPE) {
            case 1:
                setRequierdFieldsForWorkingPeriodofDirectorGenerals(adapterView, position);
                break;
            case 2:
                setRequiredFieldsForWorkingProcedures(adapterView, position);
                break;
            case 6:
                setRequriedFieldsForOfficeInfos(adapterView, position);
                break;
            case 3:
                setRequriedFieldForDivisionOfficers(adapterView, position);
                break;
            case 4:
            case 5:
                setRequierdFieldsForInfoOffices(adapterView, position);
                break;
            case 7:
            case 9:
                setRequierdFieldsForInformation(adapterView, position);
                break;
            case 8:
                setRequierdFieldsForInformationOfficers(adapterView, position);
                break;
            case 10:
            case 14:
                setRequierdFieldsForNewsUpdate(adapterView, position);
                break;
            case 11:
            case 12:
            case 13:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
                setRequierdFieldsForInfoNotices(adapterView, position);
                break;
            case 24:
                setRequiredFieldsForOfficersList(adapterView, position);
                break;
        }
    }

    private void setRequriedFieldForDivisionOfficers(View adapterView, int position) {
        final TextView txtName = (TextView) adapterView.findViewById(R.id.txtName);
        TextView txtDesignation = (TextView) adapterView.findViewById(R.id.txtDesignation);
        final TextView txtAddress = (TextView) adapterView.findViewById(R.id.txtAddress);
        final TextView txtPhone = (TextView) adapterView.findViewById(R.id.txtPhone);
        final TextView txtEmail = (TextView) adapterView.findViewById(R.id.txtEmail);
        TextView labelAddress = (TextView) adapterView.findViewById(R.id.labelAddress);
        String name = null;
        String designation = null;
        String district = null;
        String phone = null;
        String email = null;
        DivisionOffice divisionOffice = divisionOfficeList.get(position);
        name = getStringData(divisionOffice.getName());
        designation = getStringData(divisionOffice.getDesignation());
        district = getStringData(divisionOffice.getDivision());
        phone = getStringData(divisionOffice.getTelephone());
        email = getStringData(divisionOffice.getEmail());
        labelAddress.setText("বিভাগ");
        txtName.setText(name);
        txtDesignation.setText(designation);
        txtEmail.setText(email);
        txtAddress.setText(district);
        txtPhone.setText(phone);
        txtPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = txtPhone.getText().toString();
                if (text != null && !text.equals("")) {
                    String msg = "";
                    if (txtName.getText().toString() != null &&
                            !txtName.getText().toString().equals("")) {
                        msg = txtName.getText().toString() + "কে";
                    }
                    msg += " কল করার জন্য আপনার কল চার্জ প্রযোজ্য হবে ";
                    UserNotifiedDialog userNotifiedDialog = new
                            UserNotifiedDialog(context, "Calling Alert", msg, text);
                    userNotifiedDialog.showDialog();
                }
            }
        });
        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                    Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                            Toast.LENGTH_LONG).show();
                } else {
                    String email = txtEmail.getText().toString();
                    if (email != null && !email.equals("")) {
                        WebBrowser.sentEmails(context, email);
                    }
                }
            }
        });
    }

    private void setRequiredFieldsForOfficersList(View adapterView, int position) {
        final TextView txtName = (TextView) adapterView.findViewById(R.id.txtName);
        TextView txtDesignation = (TextView) adapterView.findViewById(R.id.txtDesignation);
        final TextView txtPhone = (TextView) adapterView.findViewById(R.id.txtPhone);
        final TextView txtEmail = (TextView) adapterView.findViewById(R.id.txtEmail);
        TextView txtFax = (TextView) adapterView.findViewById(R.id.txtFax);
        TextView txtOffice = (TextView) adapterView.findViewById(R.id.txtOffice);
        TextView txtMobile = (TextView) adapterView.findViewById(R.id.txtMobile);
        ImageView imageView = (ImageView) adapterView.findViewById(R.id.imageView);
        OfficersNameList officersNameList = officersNameListList.get(position);
        if (officersNameList != null) {
            txtName.setText(getStringData(officersNameList.getName()));
            txtDesignation.setText(getStringData(officersNameList.getDesignation()));
            txtOffice.setText(getStringData(officersNameList.getOffice()));
            txtEmail.setText(getStringData(officersNameList.getEmail()));
            txtMobile.setText(getStringData(officersNameList.getMobile()));
            txtPhone.setText(getStringData(officersNameList.getPhone()));
            txtFax.setText(getStringData(officersNameList.getFax()));
            if (position > officersImages.length) {
                Picasso.with(context)
                        .load(officersNameList.getImageLink())
                        .into(imageView);
            } else {
                Picasso.with(context)
                        .load(officersNameList.getImageLink())
                        .placeholder(officersImages[position])
                        .error(officersImages[position])
                        .into(imageView);
            }

        }

    }

    private void setRequierdFieldsForInfoNotices(View adapterView, int position) {
        ImageButton imageButton = (ImageButton) adapterView.findViewById(R.id.forwardBtn);
        TextView txtNoticeInfo = (TextView) adapterView
                .findViewById(R.id.txt_office_location);
        String noticeInfo = null;
        String urlLink = null;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 11:
                noticeInfo = rulesList.get(position).getRulsName();
                urlLink = rulesList.get(position).getUrlLink();
                break;
            case 12:
                noticeInfo = advertisementList.get(position).getRulsName();
                urlLink = advertisementList.get(position).getUrlLink();
                break;
            case 13:
                noticeInfo = noticeList.get(position).getRulsName();
                urlLink = noticeList.get(position).getUrlLink();
                break;
            case 15:
                noticeInfo = projectPedp3List.get(position).getRules();
                urlLink = projectPedp3List.get(position).getUrlLink();
                break;
            case 16:
                noticeInfo = schoolResultStaticticsList.get(position).getRules();
                urlLink = schoolResultStaticticsList.get(position).getUrlLink();
                break;
            case 17:
                noticeInfo = madrashaResultStaticticsList.get(position).getRules();
                urlLink = madrashaResultStaticticsList.get(position).getUrlLink();
                break;
            case 18:
                noticeInfo = monthlyInvestigationList.get(position).getRules();
                urlLink = monthlyInvestigationList.get(position).getUrlLink();
                break;
            case 19:
                noticeInfo = publicationsList.get(position).getRules();
                urlLink = publicationsList.get(position).getUrlLink();
                break;
            case 20:
                noticeInfo = formsList.get(position).getRules();
                urlLink = formsList.get(position).getUrlLink();
                break;
            case 21:
                noticeInfo = centralEServicesList.get(position).getRulsName();
                urlLink = centralEServicesList.get(position).getUrlLink();
                break;
            case 22:
                noticeInfo = internalServicesList.get(position).getRulsName();
                urlLink = internalServicesList.get(position).getUrlLink();
                break;
            case 23:
                noticeInfo = importantLinkList.get(position).getRulsName();
                urlLink = importantLinkList.get(position).getUrlLink();
                break;

        }
        txtNoticeInfo.setText(noticeInfo);
        final String FINAL_URL_LINK = urlLink;
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FINAL_URL_LINK != null && !FINAL_URL_LINK.equals("null")
                        && !FINAL_URL_LINK.equals("") && !FINAL_URL_LINK.equals("নাই")) {
                    if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                        Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                                Toast.LENGTH_LONG).show();
                    } else {
                        WebBrowser.openWebBrowser(context, FINAL_URL_LINK);
                    }
                }
            }
        });
        txtNoticeInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FINAL_URL_LINK != null && !FINAL_URL_LINK.equals("null")
                        && !FINAL_URL_LINK.equals("") && !FINAL_URL_LINK.equals("নাই")) {
                    if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                        Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                                Toast.LENGTH_LONG).show();
                    } else {
                        WebBrowser.openWebBrowser(context, FINAL_URL_LINK);
                    }
                }
            }
        });
    }

    private void setRequierdFieldsForInfoOffices(View adapterView, int position) {
        final TextView txtName = (TextView) adapterView.findViewById(R.id.txtName);
        TextView txtDesignation = (TextView) adapterView.findViewById(R.id.txtDesignation);
        final TextView txtAddress = (TextView) adapterView.findViewById(R.id.txtAddress);
        final TextView txtPhone = (TextView) adapterView.findViewById(R.id.txtPhone);
        final TextView txtEmail = (TextView) adapterView.findViewById(R.id.txtEmail);
        TextView txtFax = (TextView) adapterView.findViewById(R.id.txtFax);
        TextView labelAddress = (TextView) adapterView.findViewById(R.id.labelAddress);
        String name = null;
        String designation = null;
        String district = null;
        String phone = null;
        String email = null;
        String fax = null;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 3:
                DivisionOffice divisionOffice = divisionOfficeList.get(position);
                name = getStringData(divisionOffice.getName());
                designation = getStringData(divisionOffice.getDesignation());
                district = getStringData(divisionOffice.getDivision());
                phone = getStringData(divisionOffice.getTelephone());
                email = getStringData(divisionOffice.getEmail());
                fax = getStringData(divisionOffice.getFax());
                labelAddress.setText("বিভাগ");
                break;
            case 4:
                DistrictOffice districtOffice = districtOfficeList.get(position);
                name = getStringData(districtOffice.getName());
                designation = getStringData(districtOffice.getDesignation());
                district = getStringData(districtOffice.getDistrict());
                phone = getStringData(districtOffice.getTelephone());
                email = getStringData(districtOffice.getEmail());
                fax = getStringData(districtOffice.getFax());
                break;
            case 5:
                PtiOffice ptiOffice = ptiOfficeList.get(position);
                name = getStringData(ptiOffice.getName());
                designation = getStringData(ptiOffice.getDesignation());
                district = getStringData(ptiOffice.getDistrict());
                phone = getStringData(ptiOffice.getTelephone());
                email = getStringData(ptiOffice.getEmail());
                fax = getStringData(ptiOffice.getFax());
                break;
        }
        txtName.setText(name);
        txtDesignation.setText(designation);
        txtEmail.setText(email);
        txtFax.setText(fax);
        txtAddress.setText(district);
        txtPhone.setText(phone);
        txtPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = txtPhone.getText().toString();
                if (text != null && !text.equals("")) {
                    String msg = "";
                    if (txtName.getText().toString() != null &&
                            !txtName.getText().toString().equals("")) {
                        msg = txtName.getText().toString() + "কে";
                    }
                    msg += " কল করার জন্য আপনার কল চার্জ প্রযোজ্য হবে ";
                    UserNotifiedDialog userNotifiedDialog = new
                            UserNotifiedDialog(context, "Calling Alert", msg, text);
                    userNotifiedDialog.showDialog();
                }
            }
        });
        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                    Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                            Toast.LENGTH_LONG).show();
                } else {
                    String email = txtEmail.getText().toString();
                    if (email != null && !email.equals("")) {
                        WebBrowser.sentEmails(context, email);
                    }
                }
            }
        });
    }

    private String getStringData(String data) {
        String res = null;
        if (data != null && !data.equals("null") && !data.equals("") && !data.equals("নাই")) {
            res = data;
        }
        return res;
    }

    private void setRequierdFieldsForNewsUpdate(View adapterView, int position) {
        TextView txtHeadline = (TextView) adapterView.findViewById(R.id.txt_category);
        TextView txtDate = (TextView) adapterView.findViewById(R.id.txt_details);
        switch (LIST_VIEW_SHOW_TYPE) {
            case 10:
                NewsUpdate newsUpdate = newsUpdateList.get(position);
                if (newsUpdate != null) {
                    txtHeadline.setText(newsUpdate.getHeadline());
                    txtDate.setText("তারিখ:" + newsUpdate.getDate());
                }
                break;
            case 14:
                TenderNotice tenderNotice = tenderNoticeList.get(position);
                if (tenderNotice != null) {
                    txtHeadline.setText(tenderNotice.getTenderName());
                    txtDate.setText("তারিখ:" + tenderNotice.getDate());
                }
                break;
        }

    }

    private void setRequierdFieldsForInformationOfficers(View adapterView, int position) {
        final TextView txtName = (TextView) adapterView.findViewById(R.id.txtName);
        TextView txtDesignation = (TextView) adapterView.findViewById(R.id.txtDesignation);
        TextView txtAddress = (TextView) adapterView.findViewById(R.id.txtAddress);
        final TextView txtPhone = (TextView) adapterView.findViewById(R.id.txtPhone);
        final TextView txtEmail = (TextView) adapterView.findViewById(R.id.txtEmail);
        InfoProvidingOfficer infoProvidingOfficer = infoProvidingOfficerList.get(position);
        if (infoProvidingOfficer != null) {
            txtName.setText(infoProvidingOfficer.getName());
            txtAddress.setText(infoProvidingOfficer.getAddress());
            txtEmail.setText(infoProvidingOfficer.getEmail());
            txtDesignation.setText(infoProvidingOfficer.getDesignation());
            if (!infoProvidingOfficer.getPhone().equals("null")
                    && !infoProvidingOfficer.getPhone().equals("নাই") &&
                    !infoProvidingOfficer.getPhone().equals("") &&
                    infoProvidingOfficer.getPhone() != null) {
                txtPhone.setText(infoProvidingOfficer.getPhone());
            }
        }
        txtPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = txtPhone.getText().toString();
                if (text != null && !text.equals("")) {
                    String msg = "";
                    if (txtName.getText().toString() != null &&
                            !txtName.getText().toString().equals("")) {
                        msg = txtName.getText().toString() + "কে";
                    }
                    msg += " কল করার জন্য আপনার কল চার্জ প্রযোজ্য হবে ";
                    UserNotifiedDialog userNotifiedDialog = new
                            UserNotifiedDialog(context, "Calling Alert", msg, text);
                    userNotifiedDialog.showDialog();
                }
            }
        });
        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                    Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                            Toast.LENGTH_LONG).show();
                } else {
                    String email = txtEmail.getText().toString();
                    if (email != null && !email.equals("")) {
                        WebBrowser.sentEmails(context, email);
                    }
                }
            }
        });
    }

    private void setRequierdFieldsForInformation(View adapterView, int position) {
        ImageButton imageButton = (ImageButton) adapterView.findViewById(R.id.forwardBtn);
        TextView txtOfficePosition = (TextView) adapterView
                .findViewById(R.id.txt_office_location);
        String information = null;
        String url = null;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 7:
                information = informationRightsList.get(position).getInfo();
                url = informationRightsList.get(position).getUrlLink();
                break;
            case 9:
                information = givenseRedressSystemList.get(position).getInfo();
                url = givenseRedressSystemList.get(position).getUrlLink();
                break;
        }
        txtOfficePosition.setText(information);
        final String FINAL_URL_LINK = url;
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FINAL_URL_LINK != null && !FINAL_URL_LINK.equals("null")
                        && !FINAL_URL_LINK.equals("") && !FINAL_URL_LINK.equals("নাই")) {
                    if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                        Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                                Toast.LENGTH_LONG).show();
                    } else {
                        WebBrowser.openWebBrowser(context, FINAL_URL_LINK);
                    }
                }
            }
        });
        txtOfficePosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FINAL_URL_LINK != null && !FINAL_URL_LINK.equals("null")
                        && !FINAL_URL_LINK.equals("") && !FINAL_URL_LINK.equals("নাই")) {
                    if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                        Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                                Toast.LENGTH_LONG).show();
                    } else {
                        WebBrowser.openWebBrowser(context, FINAL_URL_LINK);
                    }
                }
            }
        });
    }

    private void setRequriedFieldsForOfficeInfos(View adapterView, int position) {
        ImageButton imageButton = (ImageButton) adapterView.findViewById(R.id.forwardBtn);
        TextView txtOfficePosition = (TextView) adapterView
                .findViewById(R.id.txt_office_location);
        String locationName = null;
        String url = null;
        switch (LIST_VIEW_SHOW_TYPE) {
            case 6:
                locationName = e_primarySystems.get(position).getName();
                url = e_primarySystems.get(position).getUrlLink();
                break;
        }
        txtOfficePosition.setText(locationName);
        final String FINAL_URL_LINK = url;
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FINAL_URL_LINK != null && !FINAL_URL_LINK.equals("null")
                        && !FINAL_URL_LINK.equals("") && !FINAL_URL_LINK.equals("নাই")) {
                    if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                        Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Intent intent = new Intent(context, FragmentActivity.class);
                        intent.putExtra("Fragment Name", "প্রাথমিক বিদ্যালয়");
                        intent.putExtra("Web Url", FINAL_URL_LINK);
                        context.startActivity(intent);
                    }
                }
            }
        });
        txtOfficePosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FINAL_URL_LINK != null && !FINAL_URL_LINK.equals("null")
                        && !FINAL_URL_LINK.equals("") && !FINAL_URL_LINK.equals("নাই")) {
                    if (!ConnectivityInfo.isInternetConnectionOn(context)) {
                        Toast.makeText(context, "আপনার ইন্টারনেট সংযোগ বন্ধ",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Intent intent = new Intent(context, FragmentActivity.class);
                        intent.putExtra("Fragment Name", "প্রাথমিক বিদ্যালয়");
                        intent.putExtra("Web Url", FINAL_URL_LINK);
                        context.startActivity(intent);
                    }
                }
            }
        });

    }


    private void setRequiredFieldsForWorkingProcedures(View adapterView, int position) {
        TextView textView = (TextView) adapterView.findViewById(R.id.txt_primary_info);
        textView.setText(workingProcedureList.get(position).getDetails().trim());
    }

    private void setRequierdFieldsForWorkingPeriodofDirectorGenerals
            (View adapterView, int position) {
        WorkingPeriodDirectorGeneral workingPeriodDirectorGeneral =
                workingPeriodDirectorGeneralList.get(position);
        TextView txtDirectorGeneralName = (TextView) adapterView
                .findViewById(R.id.txtDirectorGeneralName);
        TextView txtWorkingPeriod = (TextView) adapterView
                .findViewById(R.id.txtWorkingPeriod);
        txtDirectorGeneralName.setText(workingPeriodDirectorGeneral.getName());
        String workingPeriod = workingPeriodDirectorGeneral.getFromDate() + " - ";
        if (workingPeriodDirectorGeneral.getToDate() != null &&
                !workingPeriodDirectorGeneral.getToDate().equals("null") &&
                !workingPeriodDirectorGeneral.getToDate().equals("নাই") &&
                !workingPeriodDirectorGeneral.getToDate().equals("")) {
            workingPeriod += workingPeriodDirectorGeneral.getToDate();
        }
        txtWorkingPeriod.setText(workingPeriod);
    }
}
