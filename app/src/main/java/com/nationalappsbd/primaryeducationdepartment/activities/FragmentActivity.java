package com.nationalappsbd.primaryeducationdepartment.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.NewsUpdate;
import com.nationalappsbd.primaryeducationdepartment.DataBaseTables.PrimaryEducationDepartmentInfo;
import com.nationalappsbd.primaryeducationdepartment.R;
import com.nationalappsbd.primaryeducationdepartment.fragments.PrimaryEducationDepartmentInGoogleMapFragment;
import com.nationalappsbd.primaryeducationdepartment.fragments.PrimaryEducationDepartmentNecessaryInfoFragment;
import com.nationalappsbd.primaryeducationdepartment.fragments.WebViewFragment;
import com.nationalappsbd.primaryeducationdepartment.interfaces.Initializer;

public class FragmentActivity extends ActionBarActivity implements Initializer {

    private FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            setContentView(R.layout.frame_layout_container_activity);
            initialize();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_fragment_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            Log.e(getClass().getName(), "Back to previous activity");

        }
        return true;
    }

    @Override
    public void initialize() {
        Toolbar actionBarToolbar = (Toolbar) findViewById(R.id.actionbarToolbar);
        TextView toolbarTextView = (TextView) actionBarToolbar.findViewById(R.id.toolbar_title);
        actionBarToolbar.setTitle("");
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/BenSenHandwriting.ttf");
        toolbarTextView.setTypeface(typeface);
        Intent intent = getIntent();
        fragmentManager = getSupportFragmentManager();
        String fragmentName = intent.getStringExtra("Fragment Name");
        setFragment(fragmentName);
        toolbarTextView.setText(fragmentName);
        setSupportActionBar(actionBarToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    private void setFragment(String fragmentName) {
        Fragment fragment = null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Log.e(getClass().getName(), fragmentName);
        switch (fragmentName) {
            case "অভ্যন্তরীণ ই-সেবা":
                Log.e(getClass().getName(), fragmentName);
                fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                        .getNewInstance(12, "Internal Services");
                break;
            case "টেন্ডার ও কোটেশান":

                fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                        .getNewInstance(8, "Tender");
                break;
            case "কেন্দ্রীয় ই-সেবা":
                fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                        .getNewInstance(13, "Central E Services");
                break;
            case "গুরুত্বপূর্ণ লিংক":
                fragment = PrimaryEducationDepartmentNecessaryInfoFragment
                        .getNewInstance(14, "Important Links");
                break;
            case "মানচিত্রে অবস্থান":
                PrimaryEducationDepartmentInfo primaryEducationDepartmentInfo =
                        PrimaryEducationDepartmentInfo.getPrimaryEducationDepartmentInfo();
                fragment = PrimaryEducationDepartmentInGoogleMapFragment
                        .newInstance(primaryEducationDepartmentInfo,
                                primaryEducationDepartmentInfo.getLatitude(),
                                primaryEducationDepartmentInfo.getLongitude(),
                                10.5f);
                break;
            case "ওয়েব পেইজ":
                fragment = WebViewFragment.newInstance("http://www.dpe.gov.bd/");
                break;
            case "ওয়েব মেইল":
                fragment = WebViewFragment
                        .newInstance("https://mail.dpe.gov.bd/webmail/src/login.php");
                break;
            case "ফেইসবুক পেইজ":
                fragment = WebViewFragment
                        .newInstance("https://www.facebook.com/dpebd");
                break;
            case "খবর":
                NewsUpdate newsUpdate = (NewsUpdate) getIntent()
                        .getSerializableExtra("News Update");
                fragment = PrimaryEducationDepartmentNecessaryInfoFragment.getNewInstance(15,
                        "News Update", newsUpdate);
                break;
            case "প্রাথমিক বিদ্যালয়":
                fragment = WebViewFragment
                        .newInstance(getIntent().getStringExtra("Web Url"));
                break;
            default:
                break;

        }
        if (fragment != null && fragmentManager != null
                && fragmentTransaction != null) {
            fragmentTransaction.replace(R.id.fragment_container, fragment).commit();
        } else {
            Log.e(getClass().getName(), "Null Found");
        }
    }
}



