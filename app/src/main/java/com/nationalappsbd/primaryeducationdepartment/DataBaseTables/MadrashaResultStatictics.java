package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/25/15.
 */

@Table(name = "madrashal_result_statictics")
public class MadrashaResultStatictics extends ProjectPedp3 {

    public MadrashaResultStatictics() {
        super();
    }

    public MadrashaResultStatictics(String rulsName, String urlLink) {
        super(rulsName, urlLink);
        this.save();
    }

    public static List<MadrashaResultStatictics> getMadrashaResultStaticticsList() {
        List<MadrashaResultStatictics> madrashaResultStaticticsList = null;
        madrashaResultStaticticsList = new Select().
                from(MadrashaResultStatictics.class).
                execute();
        return madrashaResultStaticticsList;
    }
}
