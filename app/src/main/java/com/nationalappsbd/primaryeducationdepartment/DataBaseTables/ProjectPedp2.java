package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

/**
 * Created by shuvojit on 6/5/15.
 */

@Table(name = "project_pedp_2")
public class ProjectPedp2 extends Model {

    @Column(name = "info")
    private String info;

    public ProjectPedp2() {
        super();
    }

    public ProjectPedp2(String info) {
        super();
        this.info = info;
        this.save();
    }

    public static ProjectPedp2 getProjectPedp2() {
        ProjectPedp2 projectPedp2 = null;
        projectPedp2 = new Select()
                .from(ProjectPedp2.class)
                .executeSingle();
        return projectPedp2;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
