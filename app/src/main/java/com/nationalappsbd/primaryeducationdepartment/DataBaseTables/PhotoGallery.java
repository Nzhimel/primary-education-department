package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/24/15.
 */
@Table(name = "photo_gallery")
public class PhotoGallery extends Model {

    @Column(name = "image_link")
    private String imageLink;

    @Column(name = "description")
    private String description;

    public PhotoGallery() {
        super();
    }

    public PhotoGallery(String imageLink, String description) {
        super();
        this.imageLink = imageLink;
        this.description = description;
        this.save();
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static List<PhotoGallery> photoGalleryList()
    {
        List<PhotoGallery> photoGalleryList = null;
        photoGalleryList = new Select()
                .from(PhotoGallery.class)
                .execute();
        return photoGalleryList;
    }
}
