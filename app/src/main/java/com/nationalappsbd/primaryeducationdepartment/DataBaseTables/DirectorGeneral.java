package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;

/**
 * Created by shuvojit on 5/19/15.
 */

@Table(name = "director_general")
public class DirectorGeneral extends History implements Serializable{

    @Column(name = "image_link")
    private String imageLink;

    public DirectorGeneral() {
        super();
    }

    public DirectorGeneral(String details, String imageLink) {
        super(details);
        this.imageLink = imageLink;
        this.save();
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public static DirectorGeneral getDirectorGeneral()
    {
        DirectorGeneral directorGeneral = null;
        directorGeneral = new Select()
                .from(DirectorGeneral.class)
                .executeSingle();
        return directorGeneral;
    }
}
