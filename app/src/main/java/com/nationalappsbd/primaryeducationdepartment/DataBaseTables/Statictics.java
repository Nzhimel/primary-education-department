package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/25/15.
 */

@Table(name = "statictics")
public class Statictics extends Model {

    @Column(name = "sub_menu")
    public String subMenu;

    public Statictics() {
        super();
    }

    public Statictics(String subMenu) {
        super();
        this.subMenu = subMenu;
        this.save();
    }

    public String getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(String subMenu) {
        this.subMenu = subMenu;
    }

    public static List<Statictics> getStaticticsList()
    {
        List<Statictics> staticticsList = null;
        staticticsList = new Select()
                .from(Statictics.class)
                .execute();
        return staticticsList;
    }
}
