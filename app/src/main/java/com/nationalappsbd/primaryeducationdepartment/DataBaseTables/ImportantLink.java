package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/26/15.
 */

@Table(name = "important_link")
public class ImportantLink extends Rules {

    public ImportantLink() {
        super();
    }

    public ImportantLink(String rulsName, String urlLink) {
        super(rulsName, urlLink);
        this.save();
    }

    public static List<ImportantLink> getImportantLinkList() {
        List<ImportantLink> importantLinkList = null;
        importantLinkList = new Select()
                .from(ImportantLink.class)
                .execute();
        return importantLinkList;
    }
}
