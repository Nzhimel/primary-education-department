package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/23/15.
 */

@Table(name = "info_providing_officer")
public class InfoProvidingOfficer extends Model implements Serializable {

    @Column(name = "name")
    private String name;

    @Column(name = "designation")
    private String designation;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    public InfoProvidingOfficer() {
        super();
    }

    public InfoProvidingOfficer(String name, String designation,
                                String address, String phone, String email) {
        super();
        this.name = name;
        this.designation = designation;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.save();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static List<InfoProvidingOfficer> getInfoProvidingOfficerList()
    {
        List<InfoProvidingOfficer> infoProvidingOfficerList = null;
        infoProvidingOfficerList = new Select()
                .from(InfoProvidingOfficer.class)
                .execute();
        return infoProvidingOfficerList;
    }
}
