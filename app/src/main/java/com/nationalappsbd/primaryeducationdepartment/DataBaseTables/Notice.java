package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/24/15.
 */

@Table(name = "notice")
public class Notice extends Rules {

    public Notice() {
        super();
    }

    public Notice(String rulsName, String urlLink) {
        super(rulsName, urlLink);
        this.save();
    }

    public static List<Notice> getNoticeList() {
        List<Notice> noticeList = null;
        noticeList = new Select()
                .from(Notice.class)
                .execute();
        return noticeList;
    }
}
