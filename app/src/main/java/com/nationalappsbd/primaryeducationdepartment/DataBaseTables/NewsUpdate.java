package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/23/15.
 */

@Table(name = "news_update")
public class NewsUpdate extends Model implements Serializable{

    @Column(name = "headline")
    private String headline;

    @Column(name = "date")
    private String date;

    @Column(name = "details")
    private String details;

    public NewsUpdate() {
        super();
    }

    public NewsUpdate(String headline, String date, String details) {
        super();
        this.headline = headline;
        this.date = date;
        this.details = details;
        this.save();
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public static List<NewsUpdate> getNewsUpdateList() {
        List<NewsUpdate> newsUpdateList = null;
        newsUpdateList = new Select().from(NewsUpdate.class).execute();
        return newsUpdateList;
    }
}
