package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/19/15.
 */

@Table(name = "division_office")
public class DivisionOffice extends Model implements Serializable{

    @Column(name = "name")
    private String name;

    @Column(name = "designation")
    private String designation;

    @Column(name = "email")
    private String email;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "fax")
    private String fax;

    @Column(name = "division")
    private String division;

    public DivisionOffice() {
        super();
    }


    public DivisionOffice(String name, String designation,
                          String email, String telephone,
                          String fax, String division) {
        super();
        this.name = name;
        this.designation = designation;
        this.email = email;
        this.telephone = telephone;
        this.fax = fax;
        this.division = division;
        this.save();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }


    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public static List<DivisionOffice> getDivisionOfficeList()
    {
        List<DivisionOffice> divisionOfficeList = null;
        divisionOfficeList = new Select()
                .from(DivisionOffice.class)
                .execute();
        return divisionOfficeList;
    }

}
