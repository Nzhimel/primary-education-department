package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/26/15.
 */

@Table(name = "central_e_services")
public class CentralEServices extends Rules {

    public CentralEServices() {
        super();
    }

    public CentralEServices(String rulsName, String urlLink) {
        super(rulsName, urlLink);
        this.save();
    }

    public static List<CentralEServices> getCentralEServicesList() {
        List<CentralEServices> centralEServicesList = null;
        centralEServicesList = new Select().
                from(CentralEServices.class)
                .execute();
        return centralEServicesList;
    }
}
