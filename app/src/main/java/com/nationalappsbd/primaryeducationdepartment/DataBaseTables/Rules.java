package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/24/15.
 */

@Table(name = "rules")
public class Rules extends Model {

    @Column(name = "rules_name")
    private String rulsName;

    @Column(name = "url_link")
    private String urlLink;

    public Rules() {
        super();
    }

    public Rules(String rulsName, String urlLink) {
        super();
        this.rulsName = rulsName;
        this.urlLink = urlLink;
        this.save();
    }

    public String getRulsName() {
        return rulsName;
    }

    public void setRulsName(String rulsName) {
        this.rulsName = rulsName;
    }

    public String getUrlLink() {
        return urlLink;
    }

    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }

    public static List<Rules> getRulesList() {
        List<Rules> rulesList = null;
        rulesList = new Select()
                .from(Rules.class)
                .execute();
        return rulesList;
    }
}
