package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shuvojit on 5/19/15.
 */

@Table(name = "district_office")
public class DistrictOffice extends PtiOffice implements Serializable{

    public DistrictOffice() {
        super();
    }

    public DistrictOffice(String name, String designation, String email,
                          String telephone, String fax, String district) {
        super(name, designation, email, telephone, fax, district);
        this.save();
    }

    public static List<DistrictOffice> getDistrictOfficeList() {
        List<DistrictOffice> districtOfficeList = null;
        districtOfficeList = new Select()
                .from(DistrictOffice.class)
                .execute();
        return districtOfficeList;
    }
}
