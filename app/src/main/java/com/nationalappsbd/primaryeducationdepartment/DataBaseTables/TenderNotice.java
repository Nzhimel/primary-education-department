package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/25/15.
 */

@Table(name = "tender_notice")
public class TenderNotice extends Model {

    @Column(name = "tender_name")
    private String tenderName;

    @Column(name = "date")
    private String date;

    @Column(name = "url_link")
    private String urlLink;

    public TenderNotice(String tenderName, String date, String urlLink) {
        super();
        this.tenderName = tenderName;
        this.date = date;
        this.urlLink = urlLink;
        this.save();
    }

    public TenderNotice() {
        super();
    }

    public String getTenderName() {
        return tenderName;
    }

    public void setTenderName(String tenderName) {
        this.tenderName = tenderName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUrlLink() {
        return urlLink;
    }

    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }

    public static List<TenderNotice> getTenderNoticeList()
    {
        List<TenderNotice> tenderNoticeList = null;
        tenderNoticeList = new Select()
                .from(TenderNotice.class)
                .execute();
        return tenderNoticeList;
    }
}
