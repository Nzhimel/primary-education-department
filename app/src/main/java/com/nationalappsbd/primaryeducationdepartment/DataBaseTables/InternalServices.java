package com.nationalappsbd.primaryeducationdepartment.DataBaseTables;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by shuvojit on 5/26/15.
 */


@Table(name = "internal_services")
public class InternalServices extends Rules {

    public InternalServices() {
        super();
    }

    public InternalServices(String rulsName, String urlLink) {
        super(rulsName, urlLink);
        this.save();
    }

    public static List<InternalServices> getInternalServicesList()
    {
        List<InternalServices> internalServicesList = null;
        internalServicesList = new Select()
                .from(InternalServices.class)
                .execute();
        return internalServicesList;
    }
}
